package com.example.vanthang.drinkshopserver.Model

class Cart {
    var id:Int?=null
    var name:String?=null
    var link:String?=null
    var amount:Int?=null
    var price:Double?=null
    var sugar:Int?=null
    var ice:Int?=null
    var size:Int?=null
    var toppingExtras:String?=null
}