package com.example.vanthang.drinkshopserver.Utils

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import android.net.Uri
import android.os.Build
import com.example.vanthang.drinkshopserver.R

class NotificationHelper : ContextWrapper{
    var notificationManager:NotificationManager?=null
    companion object {
        const val ANT_CHANNEL_ID="com.example.vanthang.drinkshopserver.ANT"
        const val ANT_CHANNEL_NAME="Drink Shop"
    }
    constructor(base:Context):super(base)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createChannel()
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        var antChannel:NotificationChannel= NotificationChannel(ANT_CHANNEL_ID, ANT_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT)
        antChannel.enableLights(false)
        antChannel.enableVibration(true)
        antChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        getManager().createNotificationChannel(antChannel)
    }

    fun getManager():NotificationManager {
        if (notificationManager == null)
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        return notificationManager!!
    }
    @TargetApi(Build.VERSION_CODES.O)
    fun getDrinkShopNotification(title:String,
                                         message:String,
                                         soundUri: Uri):Notification.Builder
    {
        return Notification.Builder(applicationContext, ANT_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(soundUri)
                .setAutoCancel(true)
    }
}