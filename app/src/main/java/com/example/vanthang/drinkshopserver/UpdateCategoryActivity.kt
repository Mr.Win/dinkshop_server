package com.example.vanthang.drinkshopserver

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.example.vanthang.drinkshopserver.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshopserver.Utils.Common
import com.example.vanthang.drinkshopserver.Utils.ProgressRequestBody
import com.example.vanthang.drinkshopserver.Utils.UploadCallBack
import com.ipaulpro.afilechooser.utils.FileUtils
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_update_category.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import java.lang.StringBuilder
import java.util.*

class UpdateCategoryActivity : AppCompatActivity(), UploadCallBack {

    lateinit var mService: IDrinkShopAPI
    lateinit var img_browser: ImageView
    var selected_uri: Uri?=null
    var uploaded_img_path:String =""
    val PICK_FILE_REQUEST=2005

    //Rxjava
    var compositeDisposable= CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_category)

        //API
        mService = Common.getAPI()

        displayData()

        //Event
        img_browser_up_or_de.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                startActivityForResult(Intent.createChooser(FileUtils.createGetContentIntent(),"Select a file")
                        ,PICK_FILE_REQUEST)
            }
        })
        btn_update_category.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                updateCategory()
            }
        })
        btn_delete_category.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                deleteCategory()
            }
        })
    }

    private fun deleteCategory() {
        compositeDisposable.add(mService.deleteCategory(Common.current_category!!.ID!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Consumer<String> {
                    override fun accept(t: String?) {
                        Toast.makeText(this@UpdateCategoryActivity,t,Toast.LENGTH_SHORT).show()
                        uploaded_img_path=""
                        selected_uri=null
                        Common.current_category=null
                        finish()
                    }
                }))
    }

    private fun updateCategory() {
       if(!edt_name_up_or_de.text.toString().isEmpty())
       {
           compositeDisposable.add(mService.updateCategory(Common.current_category!!.ID!!,
                   edt_name_up_or_de.text.toString(),
                   uploaded_img_path!!)
                   .subscribeOn(Schedulers.io())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(object : Consumer<String> {
                       override fun accept(t: String?) {
                           Toast.makeText(this@UpdateCategoryActivity,t,Toast.LENGTH_SHORT).show()
                           uploaded_img_path=""
                           selected_uri=null
                           Common.current_category=null
                           finish()
                       }
                   }))
       }
    }

    private fun displayData() {
        if (Common.current_category !=null)
        {
            Picasso.get()
                    .load(Common.current_category!!.Link)
                    .into(img_browser_up_or_de)
            edt_name_up_or_de.setText(Common.current_category!!.Name!!)
            uploaded_img_path= Common.current_category!!.Link!!
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode==PICK_FILE_REQUEST)
        {
            if (resultCode== Activity.RESULT_OK && data !=null)
            {
                selected_uri=data.data
                if (!selected_uri!!.path.isEmpty())
                {
                    img_browser.setImageURI(selected_uri)
                    uploadFileToServer()
                }else
                    Toast.makeText(this@UpdateCategoryActivity,"Can not upload file to server", Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun uploadFileToServer() {
        if (selected_uri !=null)
        {
            var file=FileUtils.getFile(this@UpdateCategoryActivity,selected_uri)
            var fileName= StringBuilder(UUID.randomUUID().toString())
                    .append(FileUtils.getExtension(file.toString())).toString()
            var requestFile= ProgressRequestBody(file,this@UpdateCategoryActivity)
            var body= MultipartBody.Part.createFormData("uploaded_file",fileName,requestFile)
            Thread(object :Runnable{
                override fun run() {
                    mService.uploadCategoryFile(body)
                            .enqueue(object :retrofit2.Callback<String>{
                                override fun onFailure(call: Call<String>, t: Throwable) {
                                    Toast.makeText(this@UpdateCategoryActivity,t.message,Toast.LENGTH_SHORT).show()
                                }

                                override fun onResponse(call: Call<String>, response: Response<String>) {
                                    //After uploaded, we will get file name and return String contains link of image.
                                    uploaded_img_path= StringBuilder(Common.BASE_URL)
                                            .append("server/category/category_img/")
                                            .append(response.body().toString())
                                            .toString()
                                    Log.d("IMGPath",uploaded_img_path)
                                }
                            })
                }
            }).start()
        }
    }
    override fun onProgressUpdate(pertantage: Long) {

    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }
}
