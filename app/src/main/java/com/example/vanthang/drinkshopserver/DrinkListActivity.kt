package com.example.vanthang.drinkshopserver

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.example.vanthang.drinkshopserver.Adapter.DrinkListAdapter
import com.example.vanthang.drinkshopserver.Model.Drink
import com.example.vanthang.drinkshopserver.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshopserver.Utils.Common
import com.example.vanthang.drinkshopserver.Utils.ProgressRequestBody
import com.example.vanthang.drinkshopserver.Utils.UploadCallBack
import com.ipaulpro.afilechooser.utils.FileUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_drink_list.*
import kotlinx.android.synthetic.main.add_product_layout.view.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import java.util.*

class DrinkListActivity : AppCompatActivity(), UploadCallBack {
    override fun onProgressUpdate(pertantage: Long) {

    }

    lateinit var drinkListAdapter: DrinkListAdapter
    lateinit var mService: IDrinkShopAPI
    lateinit var img_browser_drink: ImageView
    lateinit var edt_name_drink: EditText
    lateinit var edt_price_drink:EditText
    val PICK_FILE_REQUEST=2001
    var selected_uri: Uri?=null
    var uploaded_img_path:String =""

    //Rxjava
    var compositeDisposable= CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drink_list)

        mService= Common.getAPI()

        //show drinks
        rw_drink_list.layoutManager= GridLayoutManager(this@DrinkListActivity,2)
        rw_drink_list.hasFixedSize()
        loadListDrink(Common.current_category!!.ID)

        btn_add_drink.setOnClickListener { view ->
            showAddDrinkDialog()
        }
    }

    private fun showAddDrinkDialog() {
        var builder: AlertDialog.Builder=AlertDialog.Builder(this@DrinkListActivity)
        builder.setTitle("Add New Drink")

        var view: View = LayoutInflater.from(this@DrinkListActivity).inflate(R.layout.add_product_layout,null)

        img_browser_drink=view.findViewById(R.id.img_browser_drink)
        edt_name_drink=view.findViewById(R.id.edt_drink_name)
        edt_price_drink=view.findViewById(R.id.edt_drink_price)


        //Event
        img_browser_drink.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                startActivityForResult(Intent.createChooser(FileUtils.createGetContentIntent(),"Select a file")
                        ,PICK_FILE_REQUEST)
            }
        })


        builder.setNegativeButton("CANCEL",object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
                uploaded_img_path=""
                selected_uri=null
            }
        }).setPositiveButton("ADD",object :DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if (edt_name_drink.text.toString().isEmpty())
                {
                    Toast.makeText(this@DrinkListActivity,"Please enter name of drink !",Toast.LENGTH_SHORT).show()
                    return
                }
                if (edt_price_drink.text.toString().isEmpty())
                {
                    Toast.makeText(this@DrinkListActivity,"Please enter price of drink !",Toast.LENGTH_SHORT).show()
                    return
                }
                if (uploaded_img_path.isEmpty())
                {
                    Toast.makeText(this@DrinkListActivity,"Please select image of drink !",Toast.LENGTH_SHORT).show()
                    return
                }
                compositeDisposable.add(mService.addNewProduct(edt_name_drink.text.toString(),
                        uploaded_img_path,
                        edt_price_drink.text.toString(), Common.current_category!!.ID!!)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object :Consumer<String>{
                            override fun accept(t: String?) {
                                Toast.makeText(this@DrinkListActivity,t,Toast.LENGTH_SHORT).show()
                                loadListDrink(Common.current_category!!.ID)
                                uploaded_img_path=""
                                selected_uri=null
                            }
                        },object :Consumer<Throwable> {
                            override fun accept(t: Throwable?) {
                                if (t != null) {
                                    Toast.makeText(this@DrinkListActivity,t.message,Toast.LENGTH_SHORT).show()
                                }
                            }
                        }))
            }
        })
        builder.setView(view)
        builder.show()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode==PICK_FILE_REQUEST)
        {
            if (resultCode== Activity.RESULT_OK && data !=null)
            {
                selected_uri=data.data
                if (!selected_uri!!.path.isEmpty())
                {
                    img_browser_drink.setImageURI(selected_uri)
                    uploadFileToServer()
                }else
                    Toast.makeText(this@DrinkListActivity,"Can not upload file to server",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun uploadFileToServer() {
        if (selected_uri !=null)
        {
            var file=FileUtils.getFile(this@DrinkListActivity,selected_uri)
            var fileName=StringBuilder(UUID.randomUUID().toString())
                    .append(FileUtils.getExtension(file.toString())).toString()
            var requestFile= ProgressRequestBody(file,this@DrinkListActivity)
            var body= MultipartBody.Part.createFormData("uploaded_file",fileName,requestFile)
            Thread(object :Runnable{
                override fun run() {
                    mService.uploadProductFile(body)
                            .enqueue(object :retrofit2.Callback<String>{
                                override fun onFailure(call: Call<String>, t: Throwable) {
                                    Toast.makeText(this@DrinkListActivity,t.message,Toast.LENGTH_SHORT).show()
                                }

                                override fun onResponse(call: Call<String>, response: Response<String>) {
                                    //After uploaded, we will get file name and return String contains link of image.
                                    uploaded_img_path=StringBuilder(Common.BASE_URL)
                                            .append("server/product/product_img/")
                                            .append(response.body().toString())
                                            .toString()
                                    Log.d("IMGPath",uploaded_img_path)
                                }
                            })
                }
            }).start()
        }
    }
    private fun loadListDrink(menuid: String?) {
        compositeDisposable.add(mService.getDrinkList(menuid!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object:Consumer<List<Drink>>{
                    override fun accept(drinkList: List<Drink>?) {
                        displayListDrink(drinkList)
                    }
                } ))
    }

    private fun displayListDrink(drinkList: List<Drink>?) {
        drinkListAdapter=DrinkListAdapter(this@DrinkListActivity,drinkList!!)
        rw_drink_list.adapter=drinkListAdapter
    }

    override fun onResume() {
        super.onResume()
        loadListDrink(Common.current_category!!.ID)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
