package com.example.vanthang.drinkshopserver

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.example.vanthang.drinkshopserver.Adapter.MenuAdapter
import com.example.vanthang.drinkshopserver.Model.Category
import com.example.vanthang.drinkshopserver.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshopserver.Utils.Common
import com.example.vanthang.drinkshopserver.Utils.ProgressRequestBody
import com.example.vanthang.drinkshopserver.Utils.UploadCallBack
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.ipaulpro.afilechooser.utils.FileUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception
import java.lang.StringBuilder
import java.util.*

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, UploadCallBack {
    override fun onProgressUpdate(pertantage: Long) {

    }

    lateinit var menuAdapter:MenuAdapter
    lateinit var mService:IDrinkShopAPI
    val REQUEST_PERMISSION_CODE=1001
    val PICK_FILE_REQUEST=1002
    lateinit var img_browser:ImageView
    lateinit var edt_name:EditText
    lateinit var rw_category_drink:RecyclerView
    var selected_uri:Uri ?=null
    var uploaded_img_path:String =""

    //Rxjava
    var compositeDisposable=CompositeDisposable()

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode)
        {
            REQUEST_PERMISSION_CODE-> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this@HomeActivity,"Permission Granted ",Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(this@HomeActivity,"Permission Denied",Toast.LENGTH_SHORT).show()
            }


        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        if (ActivityCompat.checkSelfPermission(this@HomeActivity,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this@HomeActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),REQUEST_PERMISSION_CODE)
        mService=Common.getAPI()


        fab.setOnClickListener { view ->
            showAddCategoryDialog()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        //get Category drinkshop server
        rw_category_drink=findViewById(R.id.rw_category_drink) as RecyclerView
        rw_category_drink.layoutManager=GridLayoutManager(this@HomeActivity,2)
        rw_category_drink.hasFixedSize()
        getMenu()
        updateTokenToServer()
    }

    private fun updateTokenToServer() {
        FirebaseInstanceId.getInstance()
                .instanceId
                .addOnSuccessListener(object :OnSuccessListener<InstanceIdResult>{
                    override fun onSuccess(p0: InstanceIdResult?) {
                        mService.updatetoken("server_app_01",p0!!.token,"1")
                                .enqueue(object :retrofit2.Callback<String>{
                                    override fun onFailure(call: Call<String>, t: Throwable) {
                                        Log.d("DEBUG",t.message)
                                    }

                                    override fun onResponse(call: Call<String>, response: Response<String>) {
                                        Log.d("DEBUG",response.body().toString())
                                    }
                                })
                    }
                })
                .addOnFailureListener(object :OnFailureListener{
                    override fun onFailure(p0: Exception) {
                        Toast.makeText(this@HomeActivity,p0.message.toString(),Toast.LENGTH_SHORT).show()
                    }
                })
    }

    private fun showAddCategoryDialog() {
        var builder:AlertDialog.Builder=AlertDialog.Builder(this@HomeActivity)
        builder.setTitle("Add New Category")

        var view:View=LayoutInflater.from(this@HomeActivity).inflate(R.layout.add_category_layout,null)

        img_browser=view.findViewById(R.id.img_browser)
        edt_name=view.findViewById(R.id.edt_name)


        //Event
        img_browser.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                startActivityForResult(Intent.createChooser(FileUtils.createGetContentIntent(),"Select a file")
                        ,PICK_FILE_REQUEST)
            }
        })


        builder.setNegativeButton("CANCEL",object :DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
                uploaded_img_path=""
                selected_uri=null
            }
        }).setPositiveButton("ADD",object :DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if (edt_name.text.toString().isEmpty())
                {
                    Toast.makeText(this@HomeActivity,"Please enter name of category !",Toast.LENGTH_SHORT).show()
                    return
                }
                if (uploaded_img_path.isEmpty())
                {
                    Toast.makeText(this@HomeActivity,"Please select image of category !",Toast.LENGTH_SHORT).show()
                    return
                }
                compositeDisposable.add(mService.addNewCategory(edt_name.text.toString(),uploaded_img_path)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object :Consumer<String>{
                            override fun accept(t: String?) {
                                Toast.makeText(this@HomeActivity,t,Toast.LENGTH_SHORT).show()

                                getMenu()
                                uploaded_img_path=""
                                selected_uri=null
                            }
                        }))
            }
        })
        builder.setView(view)
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode==PICK_FILE_REQUEST)
        {
            if (resultCode== Activity.RESULT_OK && data !=null)
            {
                selected_uri=data.data
                if (!selected_uri!!.path.isEmpty())
                {
                    img_browser.setImageURI(selected_uri)
                    uploadFileToServer()
                }else
                    Toast.makeText(this@HomeActivity,"Can not upload file to server",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun uploadFileToServer() {
        if (selected_uri !=null)
        {
            var file=FileUtils.getFile(this@HomeActivity,selected_uri)
            var fileName=StringBuilder(UUID.randomUUID().toString())
                    .append(FileUtils.getExtension(file.toString())).toString()
            var requestFile= ProgressRequestBody(file,this@HomeActivity)
            var body=MultipartBody.Part.createFormData("uploaded_file",fileName,requestFile)
            Thread(object :Runnable{
                override fun run() {
                    mService.uploadCategoryFile(body)
                            .enqueue(object :retrofit2.Callback<String>{
                                override fun onFailure(call: Call<String>, t: Throwable) {
                                    Toast.makeText(this@HomeActivity,t.message,Toast.LENGTH_SHORT).show()
                                }

                                override fun onResponse(call: Call<String>, response: Response<String>) {
                                    //After uploaded, we will get file name and return String contains link of image.
                                    uploaded_img_path=StringBuilder(Common.BASE_URL)
                                            .append("server/category/category_img/")
                                            .append(response.body().toString())
                                            .toString()
                                    Log.d("IMGPath",uploaded_img_path)
                                }
                            })
                }
            }).start()
        }
    }

    private fun getMenu() {
        compositeDisposable.add(mService.getMenu()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :Consumer<List<Category>>{
                    override fun accept(categorys: List<Category>?) {
                        Common.menuList=categorys!!
                        loadDisplayMenu(categorys)
                    }
                }))
    }

    private fun loadDisplayMenu(categorys: List<Category>?) {
        menuAdapter=MenuAdapter(this@HomeActivity,categorys!!)
        menuAdapter.notifyDataSetChanged()
        rw_category_drink.adapter=menuAdapter
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_order -> {
                startActivity(Intent(this@HomeActivity,ShowOrderActivity::class.java))
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onResume() {
        super.onResume()
        getMenu()

    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }
}
