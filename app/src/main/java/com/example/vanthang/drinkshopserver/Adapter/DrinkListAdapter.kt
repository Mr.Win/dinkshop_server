package com.example.vanthang.drinkshopserver.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.vanthang.drinkshopserver.Interface.IItemClickListener
import com.example.vanthang.drinkshopserver.Model.Drink
import com.example.vanthang.drinkshopserver.R
import com.example.vanthang.drinkshopserver.UpdateProductAvtivity
import com.example.vanthang.drinkshopserver.Utils.Common
import com.squareup.picasso.Picasso
import java.lang.StringBuilder

class DrinkListAdapter :RecyclerView.Adapter<DrinkListAdapter.ViewHolder>{
    var context:Context?=null
    var drinkList:List<Drink>?=null
    constructor(context: Context,drinkList:List<Drink>){
        this.context=context
        this.drinkList=drinkList
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view:View=LayoutInflater.from(context).inflate(R.layout.drink_item_layout,null,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return drinkList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get()
                .load(drinkList!![position].Link)
                .into(holder.img_drink)
        holder.txt_drink_name!!.text=drinkList!![position].Name
        holder.txt_drink_price!!.text=StringBuilder("$").append(drinkList!![position].Price).toString()
        holder.setItemClickListener(object :IItemClickListener{
            override fun onClick(view: View, isLongClick: Boolean) {

                Common.current_Drink= drinkList!!.get(position)
                context!!.startActivity(Intent(context,UpdateProductAvtivity::class.java))

            }
        })
    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView),View.OnClickListener
    {


        private var itemClickListener: IItemClickListener?=null
        var img_drink:ImageView?=null
        var txt_drink_name:TextView?=null
        var txt_drink_price:TextView?=null
        init {
            img_drink=itemView.findViewById(R.id.img_drink)
            txt_drink_name=itemView.findViewById(R.id.txt_drink_name)
            txt_drink_price=itemView.findViewById(R.id.txt_drink_price)
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.itemClickListener!!.onClick(v!!,false)
        }
        fun setItemClickListener(iItemClickListener: IItemClickListener)
        {
            this.itemClickListener=iItemClickListener
        }
    }
}