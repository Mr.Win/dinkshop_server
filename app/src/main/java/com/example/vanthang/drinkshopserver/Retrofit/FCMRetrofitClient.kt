package com.example.vanthang.drinkshopserver.Retrofit

import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class FCMRetrofitClient {
    companion object {
        private var retrofit: Retrofit?=null
        fun getClient(baseUrl:String): Retrofit {
            if (retrofit==null)
            {

                retrofit= Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create(Gson()))
                        .build()
            }
            return retrofit!!
        }
    }
}