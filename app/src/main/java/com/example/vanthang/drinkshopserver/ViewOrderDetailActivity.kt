package com.example.vanthang.drinkshopserver

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.vanthang.drinkshopserver.Adapter.OrderDetailAdapter
import com.example.vanthang.drinkshopserver.Model.DataMessage
import com.example.vanthang.drinkshopserver.Model.MyResponse
import com.example.vanthang.drinkshopserver.Model.Order
import com.example.vanthang.drinkshopserver.Model.Token
import com.example.vanthang.drinkshopserver.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshopserver.Retrofit.IFCMServices
import com.example.vanthang.drinkshopserver.Utils.Common
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_view_order_detail.*
import retrofit2.Call
import retrofit2.Response
import java.lang.StringBuilder

class ViewOrderDetailActivity : AppCompatActivity() {

    lateinit var mServer:IDrinkShopAPI
    lateinit var mFCMService:IFCMServices
    var spinner_source = arrayOf("Cancelled","Placed","Processed","Shipping","Shipped")

    var compositeDisposable=CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_order_detail)

        setSupportActionBar(toolbar)
        mServer=Common.getAPI()
        mFCMService=Common.getFCMAPI()

        rw_order_detail.layoutManager=LinearLayoutManager(this@ViewOrderDetailActivity,LinearLayoutManager.VERTICAL,false)
        rw_order_detail.hasFixedSize()
        //data
        txt_order_id.text=StringBuilder("#").append(Common.current_Order!!.OrderId).toString()
        txt_order_price.text=StringBuilder("$").append(Common.current_Order!!.OderPrice).toString()
        txt_order_address.text=Common.current_Order!!.OderAddress
        txt_order_comment.text=Common.current_Order!!.OderComment

        var spinnerArrayAdapter:ArrayAdapter<String> = ArrayAdapter(this@ViewOrderDetailActivity,
                android.R.layout.simple_spinner_item,
                spinner_source)
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_order_status.adapter=spinnerArrayAdapter
        loadOrderDetail()
        setSpinnerSelectedBaseOnOrderStatus()
    }

    private fun setSpinnerSelectedBaseOnOrderStatus() {
        when(Common.current_Order!!.OderStatus)
        {
            -1 -> spinner_order_status.setSelection(0) //Cancelled
            0 -> spinner_order_status.setSelection(1) //Placed
            1 -> spinner_order_status.setSelection(2) //Processed
            2 -> spinner_order_status.setSelection(3) //Shipping
            4 -> spinner_order_status.setSelection(4) //Shipped
        }
    }

    private fun loadOrderDetail() {
        rw_order_detail.adapter=OrderDetailAdapter(this@ViewOrderDetailActivity)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_order_detail,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.menu_check)
            saveUpdateOrder()
        return true
    }

    private fun saveUpdateOrder() {
        var order_status=spinner_order_status.selectedItemPosition - 1
        compositeDisposable.add(mServer.updateOrderStatus(order_status.toString(),
                Common.current_Order!!.UserPhone!!,
                Common.current_Order!!.OrderId!!.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :Consumer<String>{
                    override fun accept(t: String?) {

                        sendOrderUpdateNotification(Common.current_Order!!,order_status)
                    }
                },object:Consumer<Throwable>{
                    override fun accept(t: Throwable?) {
                        Log.d("ERROR",t!!.message)
                    }
                }))
    }

    private fun sendOrderUpdateNotification(current_Order: Order,order_status:Int) {

        //Get token of Owner order
        mServer.getToken(current_Order.UserPhone!!,"0")
                .enqueue(object :retrofit2.Callback<Token>{
                    override fun onFailure(call: Call<Token>, t: Throwable) {
                        Toast.makeText(this@ViewOrderDetailActivity,t.message.toString(),Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<Token>, response: Response<Token>) {
                        var userToken=response.body()
                        var dataSend=HashMap<String,String>()
                        dataSend.put("title","Your order has been update");
                        dataSend.put("message","Order #"+current_Order.OrderId+" has been update to "+Common.convertCodeToStatus(order_status))
                        var dataMessage=DataMessage()
                        dataMessage.data=dataSend
                        dataMessage.to=userToken!!.Token
                        mFCMService.sendNotification(dataMessage)
                                .enqueue(object :retrofit2.Callback<MyResponse>{
                                    override fun onFailure(call: Call<MyResponse>, t: Throwable) {
                                        Toast.makeText(this@ViewOrderDetailActivity,t.message.toString(),Toast.LENGTH_SHORT).show()
                                    }

                                    override fun onResponse(call: Call<MyResponse>, response: Response<MyResponse>) {
                                        if (response.body()!!.success == 1)
                                        {
                                            Toast.makeText(this@ViewOrderDetailActivity,"Order Update !",Toast.LENGTH_SHORT).show()
                                            finish()
                                        }
                                    }
                                })
                    }
                })
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
