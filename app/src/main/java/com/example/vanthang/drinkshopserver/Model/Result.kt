package com.example.vanthang.drinkshopserver.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Result {
    @SerializedName("message_id")
    @Expose
    var message_id:String?=null
}