package com.example.vanthang.drinkshopserver.Utils

import com.example.vanthang.drinkshopserver.Model.Category
import com.example.vanthang.drinkshopserver.Model.Drink
import com.example.vanthang.drinkshopserver.Model.Order
import com.example.vanthang.drinkshopserver.Retrofit.FCMRetrofitClient
import com.example.vanthang.drinkshopserver.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshopserver.Retrofit.IFCMServices
import com.example.vanthang.drinkshopserver.Retrofit.RetrofitClient

class Common {
    companion object {
        var current_category:Category?=null
        var current_Drink:Drink?=null
        var current_Order:Order?=null
        var menuList:List<Category> = ArrayList()
        const val BASE_URL:String="http://10.0.128.104/drinkshop/"//10.0.128.104
        const val FCM_URL:String="https://fcm.googleapis.com/"

        fun getAPI():IDrinkShopAPI
        {
            return RetrofitClient.getClient(BASE_URL).create(IDrinkShopAPI::class.java)
        }
        fun getFCMAPI():IFCMServices
        {
            return FCMRetrofitClient.getClient(FCM_URL).create(IFCMServices::class.java)
        }
        fun convertCodeToStatus(oderStatus: Int?): Any {
            when(oderStatus)
            {
                0   -> return "Placed"
                1   -> return "Processing"
                2   -> return "Shipping"
                3   -> return "Shipped"
                -1  -> return "Cancelled"
                else -> return "Order Error"
            }
        }
    }
}