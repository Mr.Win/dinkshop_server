package com.example.vanthang.drinkshopserver.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.vanthang.drinkshopserver.Model.Cart
import com.example.vanthang.drinkshopserver.R
import com.example.vanthang.drinkshopserver.Utils.Common
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import java.lang.StringBuilder

class OrderDetailAdapter :RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {
    var context:Context?=null
    var itemList:List<Cart>?=null
    constructor(context:Context)
    {
        this.context=context
        this.itemList= Gson().fromJson(Common.current_Order!!.OderDetail,object :TypeToken<List<Cart>>(){}.type)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetailAdapter.ViewHolder {
        var view=LayoutInflater.from(context).inflate(R.layout.item_order_detail_layout,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    override fun onBindViewHolder(holder: OrderDetailAdapter.ViewHolder, position: Int) {
        Picasso.get()
                .load(itemList!![position].link)
                .into(holder.img_order)
        holder.txt_drink_name!!.text=itemList!![position].name
        holder.txt_drink_amount!!.text=itemList!![position].amount.toString()
        holder.txt_sugar_ice!!.text=StringBuilder("Sugar: ").append(itemList!![position].sugar)
                .append(", ")
                .append("Ice: ").append(itemList!![position].ice)
        holder.txt_size!!.text=if (itemList!![position].size==0) "Size M" else "Size L"
        if (itemList!![position].toppingExtras !=null)
            holder.txt_topping!!.text=itemList!![position].toppingExtras
        else
            holder.txt_topping!!.text="None"

    }
    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        var img_order:ImageView?=null
        var txt_drink_name:TextView?=null
        var txt_drink_amount:TextView?=null
        var txt_sugar_ice:TextView?=null
        var txt_size:TextView?=null
        var txt_topping:TextView?=null
        init {
            img_order=itemView.findViewById(R.id.img_order)
            txt_drink_name=itemView.findViewById(R.id.txt_drink_name)
            txt_drink_amount=itemView.findViewById(R.id.txt_drink_amount)
            txt_sugar_ice=itemView.findViewById(R.id.txt_sugar_ice)
            txt_size=itemView.findViewById(R.id.txt_size)
            txt_topping=itemView.findViewById(R.id.txt_topping)
        }
    }
}