package com.example.vanthang.drinkshopserver.Interface

import android.view.View

interface IItemClickListener {
    fun onClick(view: View,isLongClick:Boolean)
}