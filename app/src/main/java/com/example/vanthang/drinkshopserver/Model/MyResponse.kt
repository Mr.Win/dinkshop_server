package com.example.vanthang.drinkshopserver.Model

class MyResponse {
    var multicast_id:Long?=null
    var success:Int?=null
    var failure:Int?=null
    var canonical_ids:Int?=null
    var results:List<Result>?=null
}