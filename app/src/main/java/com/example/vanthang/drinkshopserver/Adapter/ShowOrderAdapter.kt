package com.example.vanthang.drinkshopserver.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.vanthang.drinkshopserver.Interface.IItemClickListener
import com.example.vanthang.drinkshopserver.Model.Order
import com.example.vanthang.drinkshopserver.R
import com.example.vanthang.drinkshopserver.Utils.Common
import com.example.vanthang.drinkshopserver.ViewOrderDetailActivity
import java.lang.StringBuilder

class ShowOrderAdapter: RecyclerView.Adapter<ShowOrderAdapter.ViewHolder> {

    var context:Context?=null
    var listOrder:List<Order>?=null

    constructor(context: Context,listOrder:List<Order>)
    {
        this.context=context
        this.listOrder=listOrder
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view=LayoutInflater.from(context).inflate(R.layout.item_order_status_layout,null,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOrder!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txt_order_id!!.text= StringBuilder("#").append(listOrder!![position].OrderId).toString()
        holder.txt_order_price!!.text= StringBuilder("$").append(listOrder!![position].OderPrice).toString()
        holder.txt_order_address!!.text=listOrder!![position].OderAddress
        holder.txt_order_comment!!.text=listOrder!![position].OderComment
        holder.txt_order_status!!.text= StringBuilder("Order Status: ").append(Common.convertCodeToStatus(listOrder!![position].OderStatus))
        holder.itemClickListener(object :IItemClickListener{
            override fun onClick(view: View, isLongClick: Boolean) {
                Common.current_Order= listOrder!!.get(position)
                context!!.startActivity(Intent(context,ViewOrderDetailActivity::class.java))
            }

        })
    }
    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView),View.OnClickListener
    {


        var txt_order_id:TextView?=null
        var txt_order_price:TextView?=null
        var txt_order_address:TextView?=null
        var txt_order_comment:TextView?=null
        var txt_order_status:TextView?=null
        var iItemClickListener:IItemClickListener?=null
        init {
            txt_order_id=itemView.findViewById(R.id.txt_show_order_id)
            txt_order_price=itemView.findViewById(R.id.txt_show_order_price)
            txt_order_address=itemView.findViewById(R.id.txt_show_order_address)
            txt_order_comment=itemView.findViewById(R.id.txt_show_order_comment)
            txt_order_status=itemView.findViewById(R.id.txt_show_order_status)
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.iItemClickListener!!.onClick(v!!,false)
        }
        fun itemClickListener(iItemClickListener:IItemClickListener)
        {
            this.iItemClickListener=iItemClickListener
        }
    }
}