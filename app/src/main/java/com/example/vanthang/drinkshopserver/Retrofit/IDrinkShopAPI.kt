package com.example.vanthang.drinkshopserver.Retrofit

import com.example.vanthang.drinkshopserver.Model.Category
import com.example.vanthang.drinkshopserver.Model.Drink
import com.example.vanthang.drinkshopserver.Model.Order
import com.example.vanthang.drinkshopserver.Model.Token
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface IDrinkShopAPI {

    @GET("getmenu.php")
    fun getMenu():Observable<List<Category>>

    /*
   * Menu Manager
   */

    @FormUrlEncoded
    @POST("server/category/add_category.php")
    fun addNewCategory(@Field("name") name:String,
                       @Field("imgPath") imgPath:String):Observable<String>


    @Multipart
    @POST("server/category/uploadcategory_img.php")
    fun uploadCategoryFile(@Part file:MultipartBody.Part):Call<String>


    @FormUrlEncoded
    @POST("server/category/updatecategory.php")
    fun updateCategory(@Field("id") id:String,
                       @Field("name")name: String,
                       @Field("imgPath")imgPath:String):Observable<String>

    @FormUrlEncoded
    @POST("server/category/deletecategory.php")
    fun deleteCategory(@Field("id") id:String):Observable<String>

    /*
    * Drink Manager
    */

    @FormUrlEncoded
    @POST("getdrink.php")
    fun getDrinkList(@Field("menuid")menuid:String) : Observable<List<Drink>>

    @FormUrlEncoded
    @POST("server/product/add_product.php")
    fun addNewProduct(@Field("name") name:String,
                      @Field("imgPath") imgPath:String,
                      @Field("price") price:String,
                      @Field("menuId")menuId:String):Observable<String>


    @Multipart
    @POST("server/product/uploadproduct_img.php")
    fun uploadProductFile(@Part file:MultipartBody.Part):Call<String>

    @FormUrlEncoded
    @POST("server/product/updateproduct.php")
    fun updateProduct(@Field("id") id:String,
                      @Field("name")name: String,
                      @Field("imgPath")imgPath:String,
                      @Field("price")price: String,
                      @Field("menuId")menuId: String):Observable<String>

    @FormUrlEncoded
    @POST("server/product/deleteproduct.php")
    fun deleteProduct(@Field("id") id:String):Observable<String>

    @FormUrlEncoded
    @POST("server/order/getorderserver.php")
    fun getOrderServerByStatus(@Field("status")status:String):Observable<List<Order>>

    //update token

    @FormUrlEncoded
    @POST("updatetoken.php")
    fun updatetoken(@Field("phone")phone:String,
                    @Field("token")token:String,
                    @Field("isServerToken")isServerToken:String) : Call<String>

    //update order status
    @FormUrlEncoded
    @POST("server/order/update_order_status.php")
    fun updateOrderStatus(@Field("status")status:String,
                          @Field("phone")phone:String,
                          @Field("orderId")orderId:String):Observable<String>

    @FormUrlEncoded
    @POST("gettoken.php")
    fun getToken(@Field("phone")phone:String,
                 @Field("isServerToken")isServerToken:String):Call<Token>
}