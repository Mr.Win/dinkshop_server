package com.example.vanthang.drinkshopserver.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.vanthang.drinkshopserver.DrinkListActivity
import com.example.vanthang.drinkshopserver.Interface.IItemClickListener
import com.example.vanthang.drinkshopserver.Model.Category
import com.example.vanthang.drinkshopserver.R
import com.example.vanthang.drinkshopserver.UpdateCategoryActivity
import com.example.vanthang.drinkshopserver.Utils.Common
import com.squareup.picasso.Picasso

class MenuAdapter:RecyclerView.Adapter<MenuAdapter.ViewHolder>{
    var context:Context?=null
    var categoryList:List<Category>?=null
    constructor(context: Context,categoryList:List<Category>)
    {
        this.context=context
        this.categoryList=categoryList
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view:View= LayoutInflater.from(context).inflate(R.layout.menu_item_layout,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return categoryList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get()
                .load(categoryList!![position].Link)
                .into(holder.img_menu)
        holder.txt_menu!!.text= categoryList!![position].Name
        holder.setItemClickListener(object :IItemClickListener{
            override fun onClick(view: View, isLongClick: Boolean) {
                if (isLongClick)
                {
                    //Assign this category to variable global
                    Common.current_category= categoryList!!.get(position)
                    //Start new activity
                    context!!.startActivity(Intent(context, UpdateCategoryActivity::class.java))
                }else
                {
                    //Assign this category to variable global
                    Common.current_category= categoryList!!.get(position)
                    //Start new activity
                    context!!.startActivity(Intent(context,DrinkListActivity::class.java))
                }
            }

        })

    }


    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView),View.OnClickListener,View.OnLongClickListener
    {


        private var itemClickListener: IItemClickListener?=null
        var img_menu: ImageView?=null
        var txt_menu: TextView?=null
        init {
            img_menu=itemView.findViewById(R.id.img_menu)
            txt_menu=itemView.findViewById(R.id.txt_menu)
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            this.itemClickListener!!.onClick(v!!,false)
        }

        override fun onLongClick(v: View?): Boolean {
            this.itemClickListener!!.onClick(v!!,true)
            return true
        }
        fun setItemClickListener(iItemClickListener: IItemClickListener)
        {
            this.itemClickListener=iItemClickListener
        }
    }
}