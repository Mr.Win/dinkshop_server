package com.example.vanthang.drinkshopserver

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.example.vanthang.drinkshopserver.Adapter.ShowOrderAdapter
import com.example.vanthang.drinkshopserver.Model.Order
import com.example.vanthang.drinkshopserver.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshopserver.Utils.Common
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_show_order.*
import java.util.*

class ShowOrderActivity : AppCompatActivity() {
    lateinit var mService:IDrinkShopAPI
    lateinit var showOrderAdapter:ShowOrderAdapter

    //RxJava
    var compositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_order)

        mService=Common.getAPI()

        rw_order_status.layoutManager=LinearLayoutManager(this@ShowOrderActivity,LinearLayoutManager.VERTICAL,false)
        rw_order_status.hasFixedSize()

        nav_show_order_status.setOnNavigationItemSelectedListener(object :BottomNavigationView.OnNavigationItemSelectedListener{
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when(item.itemId)
                {
                    R.id.order_new -> loadOrderStatus("0")
                    R.id.order_cancel -> loadOrderStatus("-1")
                    R.id.order_processing -> loadOrderStatus("1")
                    R.id.order_shipping -> loadOrderStatus("2")
                    R.id.order_done -> loadOrderStatus("3")
                }
                return true
            }
        })
        loadOrderStatus("0")
    }

    private fun loadOrderStatus(status:String) {
        compositeDisposable.add(mService.getOrderServerByStatus(status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :Consumer<List<Order>>{
                    override fun accept(listOrder: List<Order>?) {
                        displayShowOrder(listOrder)
                    }
                }))
    }

    private fun displayShowOrder(listOrder: List<Order>?) {
        Collections.reverse(listOrder)
        showOrderAdapter=ShowOrderAdapter(this@ShowOrderActivity,listOrder!!)
        rw_order_status.adapter=showOrderAdapter
    }

    override fun onResume() {
        super.onResume()
        loadOrderStatus("0")
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
