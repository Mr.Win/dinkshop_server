package com.example.vanthang.drinkshopserver

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.example.vanthang.drinkshopserver.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshopserver.Utils.Common
import com.example.vanthang.drinkshopserver.Utils.ProgressRequestBody
import com.example.vanthang.drinkshopserver.Utils.UploadCallBack
import com.ipaulpro.afilechooser.utils.FileUtils
import com.jaredrummler.materialspinner.MaterialSpinner
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_update_product_avtivity.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import java.util.*

class UpdateProductAvtivity : AppCompatActivity(), UploadCallBack {
    override fun onProgressUpdate(pertantage: Long) {

    }

    lateinit var mService: IDrinkShopAPI
    lateinit var img_browser: ImageView
    var selected_uri: Uri?=null
    var uploaded_img_path:String =""
    val PICK_FILE_REQUEST=3001

    var menu_data_fot_get_key=HashMap<String,String>()
    var menu_data_for_get_value=HashMap<String,String>()
    var selected_categotry=""

    var menudata=ArrayList<String>()

    //Rxjava
    var compositeDisposable= CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_product_avtivity)

        mService=Common.getAPI()
        selected_categotry= Common.current_Drink!!.Menuid!!



        //Event
        img_browser_product_up.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                startActivityForResult(Intent.createChooser(FileUtils.createGetContentIntent(),"Select a file")
                        ,PICK_FILE_REQUEST)
            }
        })
        spinner_menu_id.setOnItemSelectedListener { view, position, id, item ->
            selected_categotry= menu_data_fot_get_key.get(menudata.get(position))!!
        }


        btn_update_product.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                updateProduct()
            }
        })
        btn_delete_product.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                deleteProduct()
            }
        })

        setSpinnerMenu()
        displayData()
    }

    private fun deleteProduct() {
        compositeDisposable.add(mService.deleteProduct(Common.current_Drink!!.ID!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :Consumer<String>{
                    override fun accept(t: String?) {
                        Toast.makeText(this@UpdateProductAvtivity,t,Toast.LENGTH_SHORT).show()
                        finish()
                    }
                },object :Consumer<Throwable>{
                    override fun accept(t: Throwable?) {
                        Toast.makeText(this@UpdateProductAvtivity,t!!.message,Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }))
    }

    private fun updateProduct() {
       if (!edt_name_product_up.text.isEmpty() && !edt_price_product_up.text.isEmpty())
       {
           compositeDisposable.add(mService.updateProduct(Common.current_Drink!!.ID!!,
                   edt_name_product_up.text.toString(),
                   uploaded_img_path,
                   edt_price_product_up.text.toString(),
                   selected_categotry
                   )
                   .subscribeOn(Schedulers.io())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(object :Consumer<String>{
                       override fun accept(t: String?) {
                           Toast.makeText(this@UpdateProductAvtivity,t,Toast.LENGTH_SHORT).show()
                           finish()
                       }
                   },object :Consumer<Throwable>{
                       override fun accept(t: Throwable?) {
                           Toast.makeText(this@UpdateProductAvtivity,t!!.message,Toast.LENGTH_SHORT).show()
                           finish()
                       }
                   }))
       }
    }

    private fun setSpinnerMenu() {
        for (category in Common.menuList)
        {
            menu_data_fot_get_key.put(category.Name!!, category.ID!!)
            menu_data_for_get_value.put(category.ID!!, category.Name!!)

            menudata.add(category.Name!!)
        }
        spinner_menu_id.setItems(menudata)
    }
    private fun displayData() {
        if (Common.current_Drink !=null)
        {
            Picasso.get()
                    .load(Common.current_Drink!!.Link)
                    .into(img_browser_product_up)
            edt_name_product_up.setText(Common.current_Drink!!.Name!!)
            edt_price_product_up.setText(Common.current_Drink!!.Price)
            spinner_menu_id.selectedIndex=menudata.indexOf(menu_data_for_get_value[Common.current_category!!.ID])
            uploaded_img_path= Common.current_category!!.Link!!
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode==PICK_FILE_REQUEST)
        {
            if (resultCode== Activity.RESULT_OK && data !=null)
            {
                selected_uri=data.data
                if (!selected_uri!!.path.isEmpty())
                {
                    img_browser.setImageURI(selected_uri)
                    uploadFileToServer()
                }else
                    Toast.makeText(this@UpdateProductAvtivity,"Can not upload file to server", Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun uploadFileToServer() {
        if (selected_uri !=null)
        {
            var file=FileUtils.getFile(this@UpdateProductAvtivity,selected_uri)
            var fileName= StringBuilder(UUID.randomUUID().toString())
                    .append(FileUtils.getExtension(file.toString())).toString()
            var requestFile= ProgressRequestBody(file,this@UpdateProductAvtivity)
            var body= MultipartBody.Part.createFormData("uploaded_file",fileName,requestFile)
            Thread(object :Runnable{
                override fun run() {
                    mService.uploadCategoryFile(body)
                            .enqueue(object :retrofit2.Callback<String>{
                                override fun onFailure(call: Call<String>, t: Throwable) {
                                    Toast.makeText(this@UpdateProductAvtivity,t.message,Toast.LENGTH_SHORT).show()
                                }

                                override fun onResponse(call: Call<String>, response: Response<String>) {
                                    //After uploaded, we will get file name and return String contains link of image.
                                    uploaded_img_path= StringBuilder(Common.BASE_URL)
                                            .append("server/product/product_img/")
                                            .append(response.body().toString())
                                            .toString()
                                    Log.d("IMGPath",uploaded_img_path)
                                }
                            })
                }
            }).start()
        }
    }
    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }
}
